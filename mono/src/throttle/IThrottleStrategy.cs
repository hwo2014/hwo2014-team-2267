using System;

namespace YeurTekRacing
{
	interface IThrottleStrategy
	{
		double CalculateThrottle();
	}
}

