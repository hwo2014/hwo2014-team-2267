using System;

namespace YeurTekRacing
{
	class CruiseStrategy : IThrottleStrategy
	{
		private double CruiseThrottle { get; set; }

		public CruiseStrategy (double cruiseThrottle)
		{
			this.CruiseThrottle = cruiseThrottle;
		}

		public double CalculateThrottle()
		{
			return CruiseThrottle;
		}
	}
}

