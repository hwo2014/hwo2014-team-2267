using System;
using System.Collections.Generic;

class Logger : List<LogEvent>
{
}

class LogEvent
{
    public char Party { get; set; }
    public DateTime Timestamp { get; set; }
    public string MsgType { get; set; }
    public object Data { get; set; }
}
