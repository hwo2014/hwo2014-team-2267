using System;
using ServiceStack.Text;
using YeurTekRacing.Lanes;

class MsgWrapper {
    public string msgType { get; set; }
    public Object data { get; set; }

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

abstract class SendMsg {
	public string ToJson() {
		return new MsgWrapper(this.MsgType(), this.MsgData()).ToJson();
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg {
	public string name { get; set; }
	public string key { get; set; }
	public string color { get; set; }

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class JoinRace: SendMsg {
	public BotId botId { get; set; }
	public string trackName { get; set; }
	public string password { get; set; }
	public int carCount { get; set; }

	public JoinRace (string botName, string botKey, int carCount = 1, string trackName = null, string passWord = null)
	{
		this.botId = new BotId { name = botName, key = botKey };
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}

	protected override string MsgType ()
	{
		return "joinRace";
	}
}

class BotId {
	public string name { get; set; }
	public string key { get; set; }
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

class SwitchLane: SendMsg {
    private SwitchDirection direction;
    
    public SwitchLane(SwitchDirection direction) {
        this.direction = direction;
    }
    
    protected override Object MsgData() {
        return this.direction.ToString();
    }
    
    protected override string MsgType() {
        return "switchLane";
    }
}
