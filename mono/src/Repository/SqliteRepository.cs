using System;
using System.Configuration;
using YeurTekRacing.Layout;
using ServiceStack.OrmLite;
using System.Data;
using System.Collections.Generic;
using System.Linq;

namespace YeurTekRacing
{
	class SqliteRepository : IRepository
	{
		public SqliteRepository ()
		{
			this.DbFactory = new OrmLiteConnectionFactory (
				ConnectionString,
				SqliteDialect.Provider
			);

			using (IDbConnection db = DbFactory.OpenDbConnection()) {
				db.CreateTableIfNotExists<Track>();
				db.CreateTableIfNotExists<Session>();
				db.CreateTableIfNotExists<Tick>();
			}
		}

		private static string ConnectionString {
			get {
				var connection = ConfigurationManager.ConnectionStrings["DefaultDb"];
				if (connection != null)
					return connection.ConnectionString;
				return "data source=YeurTekRacing.sqlite;Version=3";
			}
		}

		protected OrmLiteConnectionFactory DbFactory { get; private set; }

		#region IRepository implementation
		public void SaveTrack (Track track, bool overwrite)
		{
			using (var db = DbFactory.OpenDbConnection()) {
				if (overwrite || db.GetByIdOrDefault<Track>(track.id) == null)
					db.Save (track);
			}
		}

		public IList<Piece> GetPieces (string trackId)
		{
			using (var db = DbFactory.OpenDbConnection()) {
				var t = db.GetByIdOrDefault<Track>(trackId);
				if (t == null)
					return null;
				return t.pieces.ToList();
			}
		}

		public long RecordSession (string trackId, short lapCount)
		{
			using (var db = DbFactory.OpenDbConnection()) {
				db.Save (new Session {
					TrackId = trackId,
					LapCount = lapCount,
					StartedAt = DateTime.UtcNow,
				});
				return db.GetLastInsertId();
			}
		}

		public long RecordTelemetry(long sessionId, int tickNumber, short pieceIndex, double positionInPiece, double angle, double speed, double throttle)
		{
			using (var db = DbFactory.OpenDbConnection()) {
				db.Save (new Tick {
					SessionId = sessionId,
					TickNumber = tickNumber,
					PieceIndex = pieceIndex,
					InPieceDistance = positionInPiece,
					Angle = angle,
					Speed = speed,
					Throttle = throttle,
				});
				return db.GetLastInsertId();
			}
		}
		#endregion

	}
}

