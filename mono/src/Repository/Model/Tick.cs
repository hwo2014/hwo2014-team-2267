using System;
using ServiceStack.OrmLite;
using ServiceStack.DataAnnotations;

namespace YeurTekRacing
{
	public class Tick
	{
		[AutoIncrement]
		public long Id { get; set; }

		[ForeignKey(typeof(Session))]
		public long SessionId { get; set; }
		public int TickNumber { get; set; }

		public short PieceIndex { get; set; }
		public double InPieceDistance { get; set; }
		public double Angle { get; set; }

		public double Speed { get; set; }
		public double Throttle { get; set; }
	}
}

