using System;
using ServiceStack.DataAnnotations;
using YeurTekRacing.Layout;

namespace YeurTekRacing
{
	public class Session
	{
		[AutoIncrement] public long Id { get; set; }

		[References(typeof(Track))]
		public string TrackId { get; set; }

		public DateTime StartedAt { get; set; }
		public DateTime? CompletedAt { get; set; }
		public short LapCount { get; set; }
	}
}

