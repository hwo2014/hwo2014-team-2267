using System;
using System.Collections.Generic;
using YeurTekRacing.Layout;

namespace YeurTekRacing
{
	interface IRepository
	{
		void SaveTrack(Track track, bool overwrite = false);
		IList<Piece> GetPieces(string trackId);

		long RecordSession(string trackId, short lapCount);
		long RecordTelemetry(long sessionId, int tickNumber, short pieceIndex, double positionInPiece, double angle, double speed, double throttle);

	}
}

