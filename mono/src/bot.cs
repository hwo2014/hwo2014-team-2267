using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using ServiceStack.Text;
using YeurTekRacing.Lanes;
using YeurTekRacing.Layout;
using YeurTekRacing;

public class Bot {
	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			//new Bot(reader, writer, new JoinRace(botName, botKey, 1, "usa"));
			new Bot(reader, writer, new Join(botName, botKey));
		}
	}

	private StreamWriter writer;

	Bot(StreamReader reader, StreamWriter writer, Join join) {
		this.writer = writer;
		string line;

		var gameActive = false;

		ILaneStrategy laneStrategy = null;
		IThrottleStrategy throttleStrategy = new CruiseStrategy(0.65);

		var logger = new Logger();

		send(join, logger);

		Race race = null;
		var sessionId = 0L;
		var currentThrottle = 0.0;
		var lastPieceIndex = 0;
		var lastPositionInPiece = 0.0;
		var currentSpeed = 0.0;
		var currentAngle = 0.0;
		string myColour = null;

		var repository = new SqliteRepository();

		while((line = reader.ReadLine()) != null) {
			MsgWrapper msg = line.FromJson<MsgWrapper>();
			logger.Add(new LogEvent
			{
			    Party = 'S',
			    MsgType = msg.msgType,
			    Data = msg.data,
			    Timestamp = DateTime.UtcNow
			});

			switch(msg.msgType) {
			case "carPositions":
				var lineObject = JsonObject.Parse (line);
				var carPositions = lineObject.ArrayObjects("data");
				var myPosition = carPositions
					.Where (x => x.Object ("id").Get ("color") == myColour)
					.FirstOrDefault();
				var gameTick = lineObject.Get<int>("gameTick");

				if (gameActive) {
					var piecePosition = myPosition.Object ("piecePosition");
					var pieceIndex = piecePosition.Get<short>("pieceIndex");
					var inPieceDistance = piecePosition.Get<double>("inPieceDistance");
					var endLaneIndex = piecePosition.Object ("lane").Get<int>("endLaneIndex");

					// We only need to send a message if gameTick is specified in the server message
					if (line.Contains ("gameTick"))
					{
						var direction = SwitchDirection.None;
						if (gameTick > 1) direction = laneStrategy.SwitchRequired(endLaneIndex, pieceIndex);
						if (direction != SwitchDirection.None)
							send(new SwitchLane(direction), logger);
						else if (gameTick > 0) {
							currentThrottle = throttleStrategy.CalculateThrottle();
							send(new Throttle(currentThrottle), logger);
						}
						else {
							send(new Ping(), logger); // This should never happen!
						}
					}

					// Now it's time to record the telemetry
					currentAngle = myPosition.Get<double>("angle");
					if (pieceIndex == lastPieceIndex)
						currentSpeed = inPieceDistance - lastPositionInPiece;
					else {
						var previousPiece = race.track.pieces[lastPieceIndex];
						var previousPieceLength = previousPiece.length;
						if (previousPieceLength == 0.0)
						{
							// This is a curved piece!
							var laneOffset = race.track.lanes.Where (x => x.index == endLaneIndex).First().distanceFromCenter;
							var signsMatch = Math.Sign (laneOffset) == Math.Sign (previousPiece.angle);
							var effectiveRadius = previousPiece.radius + (signsMatch ? -1.0 : 1.0) * laneOffset;
							previousPieceLength = Math.PI * effectiveRadius * Math.Abs(previousPiece.angle) / 180;
						}
						currentSpeed = previousPieceLength - lastPositionInPiece + inPieceDistance;
					}

					lastPieceIndex = pieceIndex;
					lastPositionInPiece = inPieceDistance;

					repository.RecordTelemetry(
						sessionId,
						gameTick,
						pieceIndex,
						inPieceDistance,
						currentAngle,
						currentSpeed,
						currentThrottle
					);
				}
				else {
					send(new Throttle(throttleStrategy.CalculateThrottle()), logger);
				}
				break;
			case "join":
				Console.WriteLine("Joined");
				send(new Ping(), logger);
				break;
			case "gameInit":
				Console.WriteLine("Race init");

				// Let's get the track data from the server response
				var data = JsonObject.Parse(line);
				race = data.Object("data").Get<Race>("race");
				Console.WriteLine ("Racing at {0} with {1} pieces (total length of {2})",
				                   race.track.name,
				                   race.track.pieces.Length,
				                   race.track.pieces.Sum (x => x.length)
				               );
				repository.SaveTrack(race.track, false);
				laneStrategy = new TwoLaneStrategy(race.track);
				sessionId = repository.RecordSession(race.track.id, (short)race.raceSession.laps);
				//send(new Ping(), logger);
				break;
			case "gameEnd":
				Console.WriteLine("Race ended");
				gameActive = false;
				send(new Ping(), logger);
				break;
			case "gameStart":
				Console.WriteLine("Race starts");
				gameActive = true;
				send(new Ping(), logger);
				break;
			case "yourCar":
				myColour = JsonObject.Parse (line).Object("data").Get ("color");
				break;
			default:
				if (line.Contains("gameTick"))
					send(new Ping(), logger);
				break;
			}
		}
		
		File.WriteAllLines(
		    string.Format("log/{0:yyyyMMdd_HHmmss}.log", DateTime.UtcNow),
		    logger.Select(x => x.ToJson()).ToArray()
		);
	}

	private void send(SendMsg msg, Logger logger = null)
	{
	    if (logger != null)
	        logger.Add(new LogEvent
	        {
	            Party = 'C',
	            Timestamp = DateTime.UtcNow,
	            Data = msg,
	            MsgType = msg.GetType().ToString()
	        });
	        
		writer.WriteLine(msg.ToJson());
	}
}
