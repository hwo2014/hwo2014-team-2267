namespace YeurTekRacing.Layout
{
    class Lane
    {
        public int distanceFromCenter { get; set; }
        public int index { get; set; }
    }
}
