namespace YeurTekRacing.Layout
{
    class Race
    {
        public Track track { get; set; }
        public Car[] cars { get; set; }
        public RaceSession raceSession { get; set; }
    }
}
