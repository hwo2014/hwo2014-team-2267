namespace YeurTekRacing.Layout
{
    class RaceSession
    {
        public int laps { get; set; }
        public int maxLapTimeMs { get; set; }
        public bool quickRace { get; set; }
    }
}
