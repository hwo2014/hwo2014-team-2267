using System.Linq;
using System;

namespace YeurTekRacing.Layout
{
    class Track
    {
        public string id { get; set; }
        public string name { get; set; }
        
        public Piece[] pieces { get; set; }
        public Lane[] lanes { get; set; }
        
        public StartingPoint startingPoint { get; set; }

		public double CalculateLength()
		{
			if (pieces == null)
				return 0.0;
			return pieces.Sum(x => x.length);
		}

		public double DistanceToCorner (int trackIndex, double progressInTrack)
		{
			if (pieces == null)
				throw new Exception ("Can not calculate distance with no track information");

			if (pieces [trackIndex].radius > 0)
				return 0.0; // We're already on a corner!

			var numPieces = pieces.Length;
			var total = pieces [trackIndex].length - progressInTrack;
			var i = trackIndex + 1;
			var nextPiece = pieces [i % numPieces];
			while (nextPiece.radius == 0) {
				total += nextPiece.length;
				i += 1;
				nextPiece = pieces[i % numPieces];
			}
			return total;
		}
    }
}
