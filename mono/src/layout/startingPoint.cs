using YeurTekRacing.Util;

namespace YeurTekRacing.Layout
{
    class StartingPoint
    {
        public Position position { get; set; }
        public double angle { get; set; }
    }
}
