namespace YeurTekRacing.Layout
{
    class Piece
    {
        public double length { get; set; }
        public bool @switch { get; set; }
        public double radius { get; set; }
        public double angle { get; set; }
    }
}
