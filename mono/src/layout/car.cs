namespace YeurTekRacing.Layout
{
    class Car
    {
        public Id id { get; set; }
        public Dimensions dimensions { get; set; }
        
        public class Id
        {
            public string name { get; set; }
            public string color { get; set; }
        }
        
        public class Dimensions
        {
            public double length { get; set; }
            public double width { get; set; }
            public double guideFlagPosition { get; set; }
        }
    }
}
