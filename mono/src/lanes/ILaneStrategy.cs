using YeurTekRacing.Layout;

namespace YeurTekRacing.Lanes
{
    interface ILaneStrategy
    {
        SwitchDirection SwitchRequired(int currentLaneIndex, int currentPiece);
    }
}
