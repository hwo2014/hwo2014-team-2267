using System;
using System.Linq;
using YeurTekRacing.Layout;

namespace YeurTekRacing.Lanes
{
    class TwoLaneStrategy : ILaneStrategy
    {
        private bool hasSwitched = false;
		private int awaitingLane;

		private Track track;
		bool isClockwise;

        public TwoLaneStrategy(Track track)
		{
			this.track = track;
			if (track != null)
				this.isClockwise = track.pieces.Sum (x => x.angle) > 0;
		}

        public SwitchDirection SwitchRequired (int currentLaneIndex, int currentPiece)
		{
			if (track == null) {
				if (!hasSwitched) {
					hasSwitched = true;
					return SwitchDirection.Right;
				}
				return SwitchDirection.None;
			} else if (currentLaneIndex == awaitingLane) {
				var currentLane = track.lanes.Where (x => x.index == currentLaneIndex).FirstOrDefault ();
				if (isClockwise && track.lanes.Any (x => x.distanceFromCenter > currentLane.distanceFromCenter)) {
					awaitingLane = track.lanes
							.Where (x => x.distanceFromCenter > currentLane.distanceFromCenter)
							.OrderBy(x => x.distanceFromCenter)
							.Select (x => x.index)
							.First ();
					return SwitchDirection.Right;
				}
				if (!isClockwise && track.lanes.Any (x => x.distanceFromCenter < currentLane.distanceFromCenter)) {
					awaitingLane = track.lanes
							.Where (x => x.distanceFromCenter < currentLane.distanceFromCenter)
							.OrderByDescending(x => x.distanceFromCenter)
							.Select (x => x.index)
							.First ();
					return SwitchDirection.Left;
				}
			}
			return SwitchDirection.None;

        }
    }
}
