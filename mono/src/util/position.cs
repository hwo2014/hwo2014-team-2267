namespace YeurTekRacing.Util
{
    class Position
    {
        public Position() { }
        public Position(double x, double y) {
            this.x = x;
            this.y = y;
        }
        
        public double x { get; set; }
        public double y { get; set; }
    }
}
